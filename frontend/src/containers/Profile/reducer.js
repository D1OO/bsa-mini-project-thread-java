import {
  SET_USER, RESET_VALIDATION_RESULTS,
  VALIDATION_FAILED, SUCCESSFUL_SUBMIT, NEW_AVATAR_SET, SUCCESSFUL_PASSWORD_RESET
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false,
        successfulPasswordReset: false
      };
    case VALIDATION_FAILED:
      return {
        ...state,
        formErrors: action.formErrors
      };
    case RESET_VALIDATION_RESULTS:
      return {
        ...state,
        formErrors: [],
        successfulSubmit: false,
        newAvatar: null
      };
    case SUCCESSFUL_SUBMIT:
      return {
        ...state,
        formErrors: [],
        successfulSubmit: true
      };
    case NEW_AVATAR_SET:
      return {
        ...state,
        newAvatar: action.avatar
      };
    case SUCCESSFUL_PASSWORD_RESET:
      return {
        ...state,
        successfulPasswordReset: true
      };
    default:
      return state;
  }
};
