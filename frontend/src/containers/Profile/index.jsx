import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Form, Grid, GridColumn, GridRow, Icon, Input, Popup, Transition } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import styles from './styles.module.scss';
import { updateUserProfile, resetValidationResults } from './actions';
import ImageSelector from '../../components/ImageSelector';

const Profile = ({ user, newAvatar, updateUserProfile: _updateUserProfile,
  resetValidationResults: resetValidation, formErrors, successfulSubmit }) => {
  const [editMode, setEditMode] = useState(false);
  const [loading, setLoading] = useState(false);

  const [status, setStatus] = useState(user.status);
  const [username, setUsername] = useState(user.username);

  const formErrorsPrev = useRef(null);
  const successfulSubmitPrev = useRef(null);

  const isStatusValid = !formErrors?.profileUpdateForm?.filter(e => e.field === 'status').length;
  const isUsernameValid = !formErrors?.profileUpdateForm?.filter(e => e.field === 'username').length;

  useEffect(() => {
    if (successfulSubmit) {
      setEditMode(false);
    }
    if (formErrorsPrev.current !== formErrors || successfulSubmitPrev.current !== successfulSubmit) {
      setLoading(false);
    }
    formErrorsPrev.current = formErrors;
    successfulSubmitPrev.current = successfulSubmit;
  }, [formErrors, successfulSubmit]);

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid centered verticalAlign="middle" textAlign="center">
        <Grid.Row centered style={{ width: '20%' }}>
          <ImageSelector initialImg={user.image} isEdited={editMode} />
        </Grid.Row>
        <Grid centered style={{ paddingTop: 60 }}>
          <Form onSubmit={() => _updateUserProfile({ status, username, newAvatar })}>
            <Grid centered style={{ width: '250px' }}>
              <GridRow stretched className={styles.formGridRow}>
                <Input
                  className={styles.formInputNotEdited}
                  type="email"
                  placeholder="Email"
                  input={(<input readOnly="readOnly" defaultValue={user.email} />)}
                  size="large"
                  icon="at"
                  iconPosition="left"
                  transparent
                />
              </GridRow>
              <GridRow stretched className={styles.formGridRow}>
                <Popup
                  content="Only digits and latin letters, 3 to 15 symbols"
                  open={!isUsernameValid}
                  disabled={isUsernameValid}
                  position="right center"
                  size="mini"
                  style={{ color: '#ff6363' }}
                  trigger={(
                    <Input
                      className={editMode ? styles.formInputEdited : styles.formInputNotEdited}
                      placeholder="Username"
                      onChange={e => setUsername(e.target.value)}
                      error={!isUsernameValid}
                      input={(formErrors?.profileUpdateForm && (
                        <input defaultValue={formErrors.profileUpdateForm
                          .filter(e => e.field === 'username')[0]?.rejectedValue}
                        />
                      )) || (!editMode && (<input readOnly="readOnly" defaultValue={user.username} />))}
                      type="text"
                      transparent
                      size="large"
                      icon="user"
                      iconPosition="left"
                    />
                  )}
                />
              </GridRow>
              <GridRow stretched className={styles.formGridRow}>
                <Popup
                  content="30 symbols max"
                  open={!isStatusValid}
                  disabled={isStatusValid}
                  position="right center"
                  style={{ color: '#ff6363' }}
                  size="mini"
                  trigger={(
                    <Input
                      className={editMode ? styles.formInputEdited : styles.formInputNotEdited}
                      placeholder="Status"
                      onChange={e => setStatus(e.target.value)}
                      error={!isStatusValid}
                      input={(formErrors?.profileUpdateForm && (
                        <input defaultValue={formErrors.profileUpdateForm
                          .filter(e => e.field === 'status')[0]?.rejectedValue}
                        />
                      ))
                        || (!editMode && (<input readOnly="readOnly" defaultValue={user.status} />))}
                      transparent
                      type="text"
                      size="large"
                      icon="comment alternate outline"
                      iconPosition="left"
                    />
                  )}
                />
              </GridRow>
              <GridRow columns="equal" centered>
                <GridColumn className={styles.formGridColumn}>
                  <Transition visible={editMode} animation="fade" duration={{ hide: 250, show: 450 }}>
                    <div>
                      <Button
                        content="Save"
                        type="submit"
                        className={styles.animatedButton}
                        positive
                        loading={loading}
                        onClick={() => setLoading(true)}
                        icon="save outline"
                      />
                    </div>
                  </Transition>
                </GridColumn>
                <GridColumn className={styles.formGridColumn}>
                  <Button
                    onClick={() => { if (formErrors) resetValidation(); setEditMode(!editMode); }}
                    className={styles.animatedButton}
                    type="button"
                    color="blue"
                  >
                    <Transition visible={!editMode} animation="fade" duration={{ hide: 500, show: 500 }}>
                      <Button.Content className={styles.aminatedButtonContent}>
                        <Icon name="edit" />
                        Edit
                      </Button.Content>
                    </Transition>
                    <Transition visible={editMode} animation="fade" duration={{ hide: 50, show: 250 }}>
                      <Button.Content className={styles.aminatedButtonContent}>
                        <Icon name="cancel" style={{ paddingRight: '4px' }} />
                        Cancel
                      </Button.Content>
                    </Transition>
                  </Button>
                </GridColumn>
              </GridRow>
            </Grid>
          </Form>
        </Grid>
      </Grid>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUserProfile: PropTypes.func.isRequired,
  resetValidationResults: PropTypes.func.isRequired,
  formErrors: PropTypes.objectOf(PropTypes.any),
  successfulSubmit: PropTypes.bool,
  newAvatar: PropTypes.string
};

Profile.defaultProps = {
  user: {},
  newAvatar: null,
  formErrors: null,
  successfulSubmit: false
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user,
  newAvatar: rootState.profile.newAvatar,
  formErrors: rootState.profile.formErrors,
  successfulSubmit: rootState.profile.successfulSubmit
});

const actions = { updateUserProfile, resetValidationResults };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
