export const SET_USER = 'PROFILE_ACTION:SET_USER';
export const SET_IS_LOADING = 'PROFILE_ACTION:SET_IS_LOADING';
export const VALIDATION_FAILED = 'PROFILE_ACTION:VALIDATION_FAILED';
export const SUCCESSFUL_SUBMIT = 'PROFILE_ACTION:SUCCESSFUL_SUBMIT';
export const RESET_VALIDATION_RESULTS = 'PROFILE_ACTION:RESET_VALIDATION_RESULTS';
export const NEW_AVATAR_SET = 'PROFILE_ACTION:NEW_AVATAR_SET';
export const SUCCESSFUL_PASSWORD_RESET = 'PROFILE_ACTION:SUCCESSFUL_PASSWORD_RESET';
