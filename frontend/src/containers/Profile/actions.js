import * as authService from 'src/services/authService';
import {
  SET_USER,
  RESET_VALIDATION_RESULTS,
  VALIDATION_FAILED,
  SUCCESSFUL_SUBMIT,
  NEW_AVATAR_SET,
  SUCCESSFUL_PASSWORD_RESET
} from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

const validationNotPassedAction = formErrors => ({
  type: VALIDATION_FAILED,
  formErrors
});

const successfulSubmitAction = () => ({
  type: SUCCESSFUL_SUBMIT
});

const resetValidationResultsAction = () => ({
  type: RESET_VALIDATION_RESULTS
});

const newAvatarSetAction = avatar => ({
  type: NEW_AVATAR_SET,
  avatar
});

const successfulPasswordResetAction = () => ({
  type: SUCCESSFUL_PASSWORD_RESET
});

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const updateUserProfile = user => async (dispatch, getRootState) => {
  const updatedUser = await authService.updateUserProfile(user)
    .catch(e => {
      if (e.status === 400) {
        dispatch(validationNotPassedAction({ profileUpdateForm: e.errors }));
      }
    });
  if (updatedUser) {
    await setUser(updatedUser)(dispatch, getRootState);
    dispatch(resetValidationResultsAction());
    dispatch(successfulSubmitAction());
  }
};

export const updateUserStatus = user => async (dispatch, getRootState) => {
  const updatedUser = await authService.updateUserStatus(user)
    .catch(e => {
      if (e.status === 400) {
        dispatch(validationNotPassedAction({ status: e.errors }));
      }
    });
  if (updatedUser) {
    await setUser(updatedUser)(dispatch, getRootState);
    dispatch(resetValidationResultsAction());
    dispatch(successfulSubmitAction());
  }
};

export const resetValidationResults = () => async dispatch => {
  dispatch(resetValidationResultsAction());
};

export const newAvatarSet = avatar => async dispatch => {
  dispatch(newAvatarSetAction(avatar));
};

export const resetPasswordRequest = email => async dispatch => {
  await authService.resetPasswordRequested(email).catch(() => {
    dispatch(validationNotPassedAction({ error: 'Something went wrong' }));
  });
  dispatch(successfulSubmitAction());
};

export const confirmResetPassword = request => async dispatch => {
  const result = await authService.confirmResetPassword(request)
    .catch(e => {
      if (e.status === 406) {
        dispatch(validationNotPassedAction({ error: 'Password restore link is outdated or invalid' }));
      } else if (e.status === 400) {
        dispatch(validationNotPassedAction({ error: 'User for this email was not found' }));
      }
    });
  if (result) {
    dispatch(resetValidationResultsAction());
    dispatch(successfulPasswordResetAction());
  }
};
