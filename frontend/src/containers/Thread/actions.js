import * as postService from 'src/services/postService';
import {
  ADD_POST,
  CANCEL_EDIT_POST,
  EDIT_POST,
  LOAD_MORE_LIKES,
  LOAD_MORE_POSTS,
  REMOVE_POST,
  RESET_LIKES_POPUP, RESET_VALIDATION_RESULTS,
  SAVE_EDITED_POST,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SUCCESSFULL_SUBMIT,
  VALIDATION_FAILED, LOAD_MORE_SEARCH_RESULTS, RESET_SEARCH, SEARCH_START, SEARCH_FINISH
} from './actionTypes';

export const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

export const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const addMoreLikesAction = (likes, hasMore, likesFilter) => ({
  type: LOAD_MORE_LIKES,
  likes,
  hasMore,
  likesFilter
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const removePostAction = postId => ({
  type: REMOVE_POST,
  postId
});

const editPostAction = postId => ({
  type: EDIT_POST,
  postId
});

const cancelEditPostAction = () => ({
  type: CANCEL_EDIT_POST
});

const saveEditedPostAction = post => ({
  type: SAVE_EDITED_POST,
  post
});

const resetLikesPopUpAction = () => ({
  type: RESET_LIKES_POPUP
});

const successfullSubmitAction = () => ({
  type: SUCCESSFULL_SUBMIT
});

const validationNotPassedAction = formErrors => ({
  type: VALIDATION_FAILED,
  formErrors
});

const resetValidationResultsAction = () => ({
  type: RESET_VALIDATION_RESULTS
});

const addMoreSearchResultsAction = (searchResults, hasMoreSearchResults, searchPostsFilter) => ({
  type: LOAD_MORE_SEARCH_RESULTS,
  searchResults,
  hasMoreSearchResults,
  searchPostsFilter
});

const resetSearchAction = () => ({
  type: RESET_SEARCH
});

const searchStartAction = () => ({
  type: SEARCH_START
});

const searchFinishAction = () => ({
  type: SEARCH_FINISH
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(loadedPost => !(posts
    && posts.some(post => loadedPost.id === post.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const removePost = postId => async dispatch => {
  await postService.removePost(postId);
  dispatch(removePostAction(postId));
};

export const editPost = postId => async dispatch => {
  dispatch(editPostAction(postId));
};

export const cancelEditPost = () => async dispatch => {
  dispatch(cancelEditPostAction());
};

export const saveEditedPost = post => async dispatch => {
  const postSaved = await postService.saveEditedPost(post);
  dispatch(saveEditedPostAction(postSaved));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = (postId, postAuthorId) => async (dispatch, getRootState) => {
  const result = await postService.likePost(postId, postAuthorId);
  const diff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: result?.switched ? Number(post.dislikeCount) - 1 : Number(post.dislikeCount)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const result = await postService.dislikePost(postId);
  const diff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
    likeCount: result?.switched ? Number(post.likeCount) - 1 : Number(post.likeCount)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const resetLikesState = () => async dispatch => {
  dispatch(resetLikesPopUpAction());
};

export const loadMorePostLikes = postId => async (dispatch, getRootState) => {
  const { posts: { likesFilter } } = getRootState();
  const pageResponse = await postService.getAllLikes({ ...likesFilter, postId });
  likesFilter.from += likesFilter.count;
  dispatch(addMoreLikesAction(pageResponse.data, pageResponse.hasMore, likesFilter));
};

export const shareByMail = (email, link, whoShared) => async dispatch => {
  const response = await postService.shareByMail({ email, link, whoShared })
    .catch(e => {
      if (e.status === 400) {
        dispatch(validationNotPassedAction({ shareByEmailForm: e.errors }));
      }
    });
  if (response === true) {
    dispatch(successfullSubmitAction());
  }
};

export const resetValidationResults = () => async dispatch => {
  dispatch(resetValidationResultsAction());
};

export const doSearch = (value, type) => async (dispatch, getRootState) => {
  const { posts: { searchPostsFilter } } = getRootState();
  let pageResponse;
  if (type === 'name') {
    pageResponse = await postService.getSearchedByAuthorSuggestions({ ...searchPostsFilter, query: value });
  } else {
    pageResponse = await postService.getSearchedByContentSuggestions({ ...searchPostsFilter, query: value });
  }
  searchPostsFilter.from += searchPostsFilter.count;
  dispatch(addMoreSearchResultsAction(pageResponse.data, pageResponse.hasMore, searchPostsFilter));
};

export const resetSearchState = () => async dispatch => {
  dispatch(resetSearchAction());
};

export const searchStarted = () => async dispatch => {
  dispatch(searchStartAction());
};

export const searchFinished = () => async dispatch => {
  dispatch(searchFinishAction());
};
