import {
  ADD_POST,
  CANCEL_EDIT_COMMENT,
  CANCEL_EDIT_POST,
  EDIT_COMMENT,
  EDIT_POST,
  LOAD_MORE_LIKES,
  LOAD_MORE_POSTS, LOAD_MORE_SEARCH_RESULTS,
  REMOVE_POST,
  RESET_LIKES_POPUP, RESET_SEARCH, RESET_VALIDATION_RESULTS,
  SAVE_EDITED_COMMENT,
  SAVE_EDITED_POST,
  SET_ALL_POSTS,
  SET_EXPANDED_POST, SUCCESSFULL_SUBMIT, VALIDATION_FAILED, SEARCH_START, SEARCH_FINISH
} from './actionTypes';

const mapComment = (action, p) => {
  p.comments.map(c => (c.id !== action.comment.id ? c : action.comment));
  return p;
};

export default (state =
{ likesFilter: { from: 0, count: 5 }, searchPostsFilter: { from: 0, count: 10 } }, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case REMOVE_POST:
      return {
        ...state,
        posts: [...state.posts.filter(p => p.id !== action.postId)]
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case EDIT_POST:
      return {
        ...state,
        editedPostId: action.postId
      };
    case CANCEL_EDIT_POST:
      return {
        ...state,
        editedPostId: null
      };
    case SAVE_EDITED_POST:
      return {
        ...state,
        editedPostId: null,
        posts: [...state.posts.map(p => (p.id !== action.post.id ? p : action.post))],
        savedPostId: action.post.id
      };
    case EDIT_COMMENT:
      return {
        ...state,
        editedCommentId: action.commentId
      };
    case CANCEL_EDIT_COMMENT:
      return {
        ...state,
        editedCommentId: null
      };
    case SAVE_EDITED_COMMENT:
      return {
        ...state,
        editedCommentId: null,
        expandedPost: mapComment(action, state.expandedPost)
      };
    case LOAD_MORE_LIKES:
      return {
        ...state,
        likes: [...(state.likes || []), ...action.likes],
        hasMoreLikes: action.hasMore,
        likesFilter: action.likesFilter
      };
    case RESET_LIKES_POPUP:
      return {
        ...state,
        likes: [],
        likesFilter: { from: 0, count: 5 },
        hasMoreLikes: true
      };
    case SUCCESSFULL_SUBMIT:
      return {
        ...state,
        formErrors: null,
        successfulSubmit: true
      };
    case VALIDATION_FAILED:
      return {
        ...state,
        formErrors: action.formErrors
      };
    case RESET_VALIDATION_RESULTS:
      return {
        ...state,
        formErrors: null,
        successfulSubmit: false
      };
    case LOAD_MORE_SEARCH_RESULTS:
      return {
        ...state,
        searchResults: [...(state.searchResults || []), ...action.searchResults],
        hasMoreSearchResults: action.hasMoreSearchResults,
        searchPostsFilter: action.searchPostsFilter,
        searchInProgress: false
      };
    case RESET_SEARCH:
      return {
        ...state,
        searchResults: [],
        searchPostsFilter: { from: 0, count: 10 },
        hasMoreSearchResults: false
      };
    case SEARCH_START:
      return {
        ...state,
        searchInProgress: true
      };
    case SEARCH_FINISH:
      return {
        ...state,
        searchInProgress: false
      };
    default:
      return state;
  }
};
