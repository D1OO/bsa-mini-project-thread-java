import React, { useEffect, useRef, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Header, Icon, Loader, Segment } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  addPost,
  cancelEditPost,
  dislikePost,
  editPost,
  likePost,
  loadMorePostLikes,
  loadMorePosts,
  loadPosts,
  removePost,
  saveEditedPost,
  toggleExpandedPost,
  shareByMail,
  resetValidationResults,
  doSearch
} from './actions';

import styles from './styles.module.scss';
import SearchPosts from '../../components/SearchPosts';

const postsFilter = {
  username: undefined,
  excludedUserId: undefined,
  likedByUserId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  username,
  loadPosts: _loadPosts,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  loadMorePostLikes: loadMoreLikes,
  toggleExpandedPost: toggle,
  removePost: remove,
  editPost: edit,
  cancelEditPost: cancelEdit,
  saveEditedPost: saveEdited,
  editedPostId: editedId,
  savedPostId: savedId,
  shareByMail: _shareByMail,
  successfulSubmit,
  formErrors,
  resetValidationResults: _resetValidationResults,
  doSearch: _doSearch,
  searchResults
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [showLikedByMe, setShowLikedByMe] = useState(false);
  const [isPostsLoading, setIsPostsLoading] = useState(false);

  const prevPosts = useRef(null);

  useEffect(() => {
    if (posts !== prevPosts.current) setIsPostsLoading(false);
  }, [posts]);

  const getMorePosts = () => {
    setIsPostsLoading(true);
    prevPosts.current = posts;
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const load = () => {
    postsFilter.from = 0;
    _loadPosts(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.username = showOwnPosts ? undefined : username;
    load();
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    postsFilter.excludedUserId = hideOwnPosts ? undefined : userId;
    load();
  };

  const toggleShowLikedByMe = () => {
    setShowLikedByMe(!showLikedByMe);
    postsFilter.likedByUserId = showLikedByMe ? undefined : userId;
    load();
  };

  const searchByUser = query => {
    postsFilter.username = query;
    delete postsFilter.content;
    load();
  };

  const searchByContent = query => {
    postsFilter.content = query;
    if (postsFilter.username !== username) delete postsFilter.username;
    load();
  };

  const resetSearch = () => {
    if (postsFilter.username !== username) delete postsFilter.username;
    delete postsFilter.content;
    load();
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
          disabled={hideOwnPosts}
        />
        <Checkbox
          toggle
          label="Hide my posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnPosts}
          disabled={showOwnPosts}
        />
        <Checkbox
          toggle
          label="Liked by me"
          checked={showLikedByMe}
          onChange={toggleShowLikedByMe}
        />
      </div>
      <SearchPosts
        doSearch={_doSearch}
        searchResults={searchResults}
        findPostsByUsername={searchByUser}
        findPostsByContent={searchByContent}
        resetSearch={resetSearch}
        toggleExpandedPost={toggle}
      />

      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.length > 0 || (posts.length === 0 && isPostsLoading) ? (posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            removePost={userId === post.user.id ? remove : null}
            isEdited={editedId === post.id}
            isSaved={savedId === post.id}
            editPost={edit}
            cancelEditPost={cancelEdit}
            saveEditedPost={saveEdited}
            loadMoreLikes={loadMoreLikes}
            key={post.id}
          />
        ))) : (
          <Segment placeholder>
            <Header icon>
              <Icon name="search" />
              Nothing is found
            </Header>
          </Segment>
        ) }
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} loadMorePostLikes={loadMoreLikes} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          whoShared={username}
          close={() => setSharedPostId(undefined)}
          shareByMail={_shareByMail}
          submit={successfulSubmit}
          validationResults={formErrors}
          resetValidationResults={_resetValidationResults}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  username: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  loadMorePostLikes: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  removePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  shareByMail: PropTypes.func.isRequired,
  editedPostId: PropTypes.string,
  cancelEditPost: PropTypes.func.isRequired,
  saveEditedPost: PropTypes.func.isRequired,
  savedPostId: PropTypes.string,
  successfulSubmit: PropTypes.bool,
  formErrors: PropTypes.objectOf(PropTypes.any),
  resetValidationResults: PropTypes.func.isRequired,
  doSearch: PropTypes.func.isRequired,
  searchResults: PropTypes.arrayOf(PropTypes.object)
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined,
  username: undefined,
  editedPostId: undefined,
  savedPostId: undefined,
  successfulSubmit: false,
  formErrors: null,
  searchResults: []
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id,
  username: rootState.profile.user.username,
  editedPostId: rootState.posts.editedPostId,
  savedPostId: rootState.posts.savedPostId,
  successfulSubmit: rootState.posts.successfulSubmit,
  formErrors: rootState.posts.formErrors,
  searchResults: rootState.posts.searchResults
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  removePost,
  editPost,
  cancelEditPost,
  saveEditedPost,
  loadMorePostLikes,
  shareByMail,
  resetValidationResults,
  doSearch
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
