import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { login, resetPasswordRequest } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Button, Grid, Header, Icon, Input, Label, Message, Popup, Transition } from 'semantic-ui-react';

import LoginForm from 'src/components/LoginForm';
import validator from 'validator';
import styles from './styles.module.scss';

const LoginPage = ({ login: signIn,
  resetPasswordRequest: _resetPasswordRequest,
  successfulPasswordReset, successfulSubmit
}) => {
  const [restoringPwd, setRestoringPwd] = useState(false);
  const [restoreEmail, setRestoreEmail] = useState(null);
  const [isRestoreEmailValid, setIsRestoreEmailValid] = useState(true);

  const submitPwdRestoreRequest = () => {
    if (isRestoreEmailValid) {
      _resetPasswordRequest(restoreEmail);
    }
  };

  return (
    <Grid textAlign="center" className="fill">
      <Grid.Row verticalAlign="middle">
        <Grid.Column style={{ maxWidth: 450 }}>
          <Logo />
          <Header as="h2" color="teal" textAlign="center">
            Login to your account
          </Header>
          {successfulPasswordReset && (
            <Message positive attached="bottom" color="green">
              <Message.Header>Success</Message.Header>
              <p>Login with your new password</p>
            </Message>
          ) }
          <LoginForm login={signIn} />
          <Message>
            New to us?
            {' '}
            <NavLink exact to="/registration">Sign Up</NavLink>
          </Message>
          <Transition visible={successfulSubmit}>
            <Message attached="bottom" color="teal">
              <p>Check your inbox for a password reset link</p>
            </Message>
          </Transition>
          {!successfulSubmit && (
            <Popup
              style={{ width: '450px', fontWeight: 'bold' }}
              flowing
              wide="very"
              on="click"
              basic
              position="bottom center"
              trigger={(
                <Label as="a" className={styles.restorePwd} onClick={() => setRestoringPwd(!restoringPwd)}>
                  {' '}
                  <Icon name="key" />
                  Reset Password
                </Label>
              )}
            >
              <Input
                icon="at"
                iconPosition="left"
                placeholder="Account e-mail address"
                fluid
                inverted
                error={!isRestoreEmailValid}
                action={<Button attached="bottom" onClick={submitPwdRestoreRequest}>Confirm</Button>}
                onChange={event => setRestoreEmail(event.target.value)}
                onBlur={() => restoreEmail && setIsRestoreEmailValid(!validator.isEmpty(restoreEmail)
                    && validator.isEmail(restoreEmail))}
              />
            </Popup>
          )}
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  resetPasswordRequest: PropTypes.func.isRequired,
  successfulPasswordReset: PropTypes.bool,
  successfulSubmit: PropTypes.bool
};

LoginPage.defaultProps = {
  successfulPasswordReset: false,
  successfulSubmit: false
};

const mapStateToProps = rootState => ({
  successfulPasswordReset: rootState.profile.successfulPasswordReset,
  successfulSubmit: rootState.profile.successfulSubmit
});

const actions = { login, resetPasswordRequest };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
