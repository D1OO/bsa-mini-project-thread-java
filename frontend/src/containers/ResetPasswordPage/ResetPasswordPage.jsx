import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink, useHistory } from 'react-router-dom';
import PasswordResetForm from '../../components/PasswordResetForm';
import { confirmResetPassword } from '../Profile/actions';

const ResetPasswordPage = ({
  match, confirmResetPassword: _confirmResetPassword, successfulPasswordReset, formErrors }) => {
  const history = useHistory();

  useEffect(() => {
    if (successfulPasswordReset) {
      history.push('/login');
    }
  });

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Password reset
        </Header>
        {formErrors && (
          <Message color="yellow" attached="bottom">
            <Message.Header>Warning</Message.Header>
            <p>{formErrors.error}</p>
          </Message>
        )}
        <PasswordResetForm
          confirmResetPassword={_confirmResetPassword}
          resetHash={match.params.resetHash}
        />
        <Message>
          <NavLink exact to="/login">Login</NavLink>
          {' '}
          or
          {' '}
          <NavLink exact to="/registration">Sign Up</NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

ResetPasswordPage.propTypes = {
  match: PropTypes.objectOf(PropTypes.any),
  confirmResetPassword: PropTypes.func.isRequired,
  successfulPasswordReset: PropTypes.bool,
  formErrors: PropTypes.objectOf(PropTypes.any)
};

ResetPasswordPage.defaultProps = {
  match: undefined,
  successfulPasswordReset: false,
  formErrors: null
};

const mapStateToProps = rootState => ({
  successfulPasswordReset: rootState.profile.successfulPasswordReset,
  formErrors: rootState.profile.formErrors
});

const actions = { confirmResetPassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPasswordPage);
