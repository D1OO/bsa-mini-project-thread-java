import * as commentService from 'src/services/commentService';
import { CANCEL_EDIT_COMMENT, EDIT_COMMENT, SAVE_EDITED_COMMENT } from '../Thread/actionTypes';
import { setExpandedPostAction, setPostsAction, addMoreLikesAction } from '../Thread/actions';

const editCommentAction = commentId => ({
  type: EDIT_COMMENT,
  commentId
});

const cancelEditCommentAction = () => ({
  type: CANCEL_EDIT_COMMENT
});

const saveEditedCommentAction = comment => ({
  type: SAVE_EDITED_COMMENT,
  comment
});

export const addComment = request => async (dispatch, getRootState) => {
  const comment = await commentService.addComment(request);
  // const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== request.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === request.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const editComment = commentId => async dispatch => {
  dispatch(editCommentAction(commentId));
};

export const cancelEditComment = () => async dispatch => {
  dispatch(cancelEditCommentAction());
};

export const saveEditedComment = comment => async dispatch => {
  const commentSaved = await commentService.saveEditedComment(comment);
  dispatch(saveEditedCommentAction(commentSaved));
};

export const removeComment = (postId, commentId) => async (dispatch, getRootState) => {
  await commentService.removeComment(commentId);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: [...(post.comments || []).filter(c => c.id !== commentId)] // commentId is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = (postId, commentId, commentAuthorId) => async (dispatch, getRootState) => {
  const result = await commentService.likeComment(commentId, commentAuthorId);
  const diff = result?.id ? 1 : -1;

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff,
    dislikeCount: result?.switched ? Number(comment.dislikeCount) - 1 : Number(comment.dislikeCount)
  });

  const mapComments = post => ({
    ...post,
    comments: post.comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)))
  });

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const dislikeComment = (postId, commentId) => async (dispatch, getRootState) => {
  const result = await commentService.dislikeComment(commentId);
  const diff = result?.id ? 1 : -1;

  const mapLikes = comment => ({
    ...comment,
    dislikeCount: Number(comment.dislikeCount) + diff,
    likeCount: result?.switched ? Number(comment.likeCount) - 1 : Number(comment.likeCount)
  });

  const mapComments = post => ({
    ...post,
    comments: post.comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)))
  });

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const loadMoreCommentLikes = commentId => async (dispatch, getRootState) => {
  const { posts: { likesFilter } } = getRootState();
  const pageResponse = await commentService.getAllLikes({ ...likesFilter, commentId });
  likesFilter.from += likesFilter.count;
  dispatch(addMoreLikesAction(pageResponse.data, pageResponse.hasMore, likesFilter));
};
