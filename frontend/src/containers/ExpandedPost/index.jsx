import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Comment as CommentUI, Header, Modal } from 'semantic-ui-react';
import moment from 'moment';
import { dislikePost, likePost, toggleExpandedPost } from 'src/containers/Thread/actions';
import {
  addComment,
  cancelEditComment,
  dislikeComment,
  loadMoreCommentLikes,
  editComment,
  likeComment,
  removeComment,
  saveEditedComment
} from 'src/containers/ExpandedPost/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  userId,
  post,
  sharePost,
  likePost: likePostExpanded,
  dislikePost: dislike,
  loadMoreCommentLikes: _loadMoreCommentLikes,
  loadMorePostLikes: _loadMorePostLikes,
  toggleExpandedPost: toggle,
  addComment: add,
  removeComment: remove,
  editComment: _editComment,
  cancelEditComment: _cancelEditComment,
  saveEditedComment: _saveEditedComment,
  likeComment: likeCommentExpanded,
  dislikeComment: dislikeCommentExpanded,
  editedCommentId: editedId
}) => (
  <Modal centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={likePostExpanded}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            loadMoreLikes={_loadMorePostLikes}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  comment={comment}
                  postId={post.id}
                  likeComment={likeCommentExpanded}
                  dislikeComment={dislikeCommentExpanded}
                  removeComment={userId === comment.user.id ? remove : null}
                  editComment={_editComment}
                  isEdited={editedId === comment.id}
                  cancelEdit={_cancelEditComment}
                  saveEdited={_saveEditedComment}
                  loadMoreLikes={_loadMoreCommentLikes}
                />
              ))}
            <AddComment postId={post.id} postAuthorId={post.user.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  userId: PropTypes.string.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  loadMorePostLikes: PropTypes.func.isRequired,
  loadMoreCommentLikes: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  removeComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  cancelEditComment: PropTypes.func.isRequired,
  saveEditedComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editedCommentId: PropTypes.string
};

ExpandedPost.defaultProps = {
  editedCommentId: undefined
};

const mapStateToProps = rootState => ({
  userId: rootState.profile.user.id,
  post: rootState.posts.expandedPost,
  editedCommentId: rootState.posts.editedCommentId
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  removeComment,
  likeComment,
  dislikeComment,
  editComment,
  cancelEditComment,
  saveEditedComment,
  loadMoreCommentLikes
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
