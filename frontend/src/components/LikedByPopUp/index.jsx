import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { Image, List, Loader, Popup } from 'semantic-ui-react';
import { resetLikesState } from '../../containers/Thread/actions';
import styles from './styles.module.scss';
import { getUserImgLink } from '../../helpers/imageHelper';

const LikedByPopUp = ({ hoverableElement, likedItemId: itemId,
  likes, hasMoreLikes, loadMoreLikes: loadMore, resetLikesState: resetState }) => {
  const contents = (
    <div className={styles.likesPopUp}>
      <InfiniteScroll
        useWindow={false}
        pageStart={0}
        threshold={20}
        loadMore={() => loadMore(itemId)}
        hasMore={hasMoreLikes}
        loader={<Loader active inline="centered" key={0} />}
      >
        {likes.map(l => (
          <List animated verticalAlign="middle">
            <List.Item key={itemId}>
              <Image avatar src={getUserImgLink(l.avatar)} />
              <List.Content>
                <List.Header>{l.username}</List.Header>
              </List.Content>
            </List.Item>
          </List>
        ))}
      </InfiniteScroll>
    </div>
  );

  return (
    <Popup
      content={contents}
      trigger={hoverableElement}
      onOpen={() => resetState()}
      header="Liked by:"
      position="bottom left"
      flowing
      hoverable
      inverted
    />
  );
};

LikedByPopUp.propTypes = {
  hoverableElement: PropTypes.objectOf(PropTypes.any).isRequired,
  likedItemId: PropTypes.string.isRequired,
  likes: PropTypes.arrayOf(PropTypes.object),
  resetLikesState: PropTypes.func.isRequired,
  loadMoreLikes: PropTypes.func.isRequired,
  hasMoreLikes: PropTypes.bool
};

LikedByPopUp.defaultProps = {
  likes: [],
  hasMoreLikes: true
};

const mapStateToProps = rootState => ({
  likes: rootState.posts.likes,
  hasMoreLikes: rootState.posts.hasMoreLikes
});

const actions = { resetLikesState };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LikedByPopUp);
