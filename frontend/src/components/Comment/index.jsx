import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import EditCommentBodyForm from '../EditedCommentBody';
import LikedByPopUp from '../LikedByPopUp';

const Comment = ({ comment: { id, body, createdAt, user, likeCount, dislikeCount }, comment,
  postId, removeComment, likeComment, dislikeComment, loadMoreLikes, editComment, isEdited, cancelEdit, saveEdited
}) => {
  const bodyBlock = () => (isEdited
    ? <EditCommentBodyForm comment={comment} saveEditedComment={saveEdited} cancelEditComment={cancelEdit} />
    : <span className="postBody" style={{ whiteSpace: 'pre-line' }}>{ body }</span>);

  const likesBlock = () => {
    const likes = (
      <CommentUI.Action onClick={() => likeComment(postId, id, user.id)}>
        <Icon name="thumbs up" />
        {likeCount}
      </CommentUI.Action>
    );
    return likeCount > 0
      ? (<LikedByPopUp likedItemId={comment.id} loadMoreLikes={loadMoreLikes} hoverableElement={(likes)} />) : likes;
  };

  return (
    <CommentUI>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Metadata style={{ display: 'block', marginLeft: 0, paddingBottom: '0.5rem' }}>
          <span style={{ fontStyle: 'italic', fontSize: '0.84rem' }}>
            {user.status}
          </span>
        </CommentUI.Metadata>
        <CommentUI.Text>
          {bodyBlock()}
        </CommentUI.Text>
        <CommentUI.Actions>
          {likesBlock()}
          <CommentUI.Action onClick={() => dislikeComment(postId, id)}>
            <Icon name="thumbs down" />
            {dislikeCount}
          </CommentUI.Action>
          {removeComment
          && (
            <>
              <CommentUI.Action onClick={() => editComment(id)}>
                <Icon name="edit" />
                Edit
              </CommentUI.Action>
              <CommentUI.Action onClick={() => removeComment(postId, id)}>
                <Icon name="remove" />
                Remove
              </CommentUI.Action>
            </>
          )}
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  postId: PropTypes.string.isRequired,
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  removeComment: PropTypes.func,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  loadMoreLikes: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  isEdited: PropTypes.bool,
  saveEdited: PropTypes.func.isRequired,
  cancelEdit: PropTypes.func.isRequired
};

Comment.defaultProps = {
  removeComment: null,
  isEdited: false
};

export default Comment;
