import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Button, Grid, Header as HeaderUI, Icon, Image, Input } from 'semantic-ui-react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './styles.module.scss';
import { resetValidationResults, updateUserStatus } from '../../containers/Profile/actions';
import { Logo } from '../Logo';

const Header = ({ user, logout,
  updateUserStatus: _updateUserStatus, resetValidationResults: _resetValidationResults,
  successfulSubmit, formErrors }) => {
  const [statusEdited, setStatusEdited] = useState(false);
  const [status, setStatus] = useState(user.status);

  const sav = () => {
    _updateUserStatus({ status });
  };

  useEffect(() => {
    if (successfulSubmit) setStatusEdited(false);
  }, [successfulSubmit]);

  return (
    <header className={styles.headerWrp}>
      <div>
        <Grid centered container columns="3">
          <Grid.Column className={styles.headerColumn}>
            {user && (
              <HeaderUI className={styles.userInfo}>
                <Image circular src={getUserImgLink(user.image)} />
                <div className={styles.userInfoNameStatus}>
                  <NavLink exact to="/" className={styles.userInfoName}>
                    {user.username}
                  </NavLink>
                  <br />
                  { statusEdited
                    ? (
                      <Input
                        transparent
                        className={styles.userInfoStatusEdited}
                        onChange={e => setStatus(e.target.value)}
                        input={formErrors?.status ? <input defaultValue={formErrors.status[0]?.rejectedValue} />
                          : <input defaultValue={status} />}
                      />
                    ) : (user.status && <span className={styles.userInfoStatus}>{user.status}</span>)}
                  {!user.status && !statusEdited && <span className={styles.emptyStatusString}>Set status</span>}
                  <Icon
                    onClick={statusEdited ? sav : () => setStatusEdited(!statusEdited)}
                    color={statusEdited ? 'green' : 'blue'}
                    name={statusEdited ? 'save' : 'pen square'}
                  />
                  { statusEdited && (
                    <Icon
                      onClick={() => { if (formErrors) _resetValidationResults(); setStatusEdited(!statusEdited); }}
                      color="red"
                      name="cancel"
                    />
                  )}
                  { formErrors?.status
                && <span className={styles.userInfoStatusError}>max length is 30 symbols</span> }
                </div>
              </HeaderUI>
            )}
          </Grid.Column>
          <Grid.Column textAlign="center" className={styles.headerColumn}>
            <Logo />
          </Grid.Column>
          <Grid.Column textAlign="right" className={styles.headerColumn}>
            <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
              <Icon name="user circle" size="large" />
            </NavLink>
            <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
              <Icon name="log out" size="large" />
            </Button>
          </Grid.Column>
        </Grid>
      </div>
    </header>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  updateUserStatus: PropTypes.func.isRequired,
  resetValidationResults: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  formErrors: PropTypes.objectOf(PropTypes.any),
  successfulSubmit: PropTypes.bool
};

Header.defaultProps = {
  formErrors: null,
  successfulSubmit: false
};

const actions = { updateUserStatus, resetValidationResults };

const mapStateToProps = rootState => ({
  formErrors: rootState.profile.formErrors,
  successfulSubmit: rootState.profile.successfulSubmit
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);

