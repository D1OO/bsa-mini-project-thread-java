import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, TextArea } from 'semantic-ui-react';

class EditCommentBodyForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      body: props.comment.body,
      loading: false
    };
    this.properties = {
      comment: props.comment,
      cancelEditComment: props.cancelEditComment,
      saveEditedComment: props.saveEditedComment
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ body: event.target.value });
  }

  handleSubmit(event) {
    const { body } = this.state;
    const { comment, saveEditedComment } = this.properties;
    comment.body = body;
    saveEditedComment(comment);
    event.preventDefault();
  }

  render() {
    const { body, loading } = this.state;
    const { cancelEditComment } = this.properties;
    return (
      <Form onSubmit={this.handleSubmit}>
        <TextArea value={body} onChange={this.handleChange} style={{ marginBottom: '1rem' }}>
          {body}
        </TextArea>
        <Button.Group floated="right" size="mini">
          <Button
            type="submit"
            compact
            positive={!loading}
            loading={loading}
            onClick={() => this.setState({ loading: true })}
          >
            Save
          </Button>
          <Button.Or />
          <Button compact onClick={() => cancelEditComment()}> Cancel </Button>
        </Button.Group>
      </Form>
    );
  }
}

EditCommentBodyForm.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  cancelEditComment: PropTypes.func.isRequired,
  saveEditedComment: PropTypes.func.isRequired
};

export default EditCommentBodyForm;
