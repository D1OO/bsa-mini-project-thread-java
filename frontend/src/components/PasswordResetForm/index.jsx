import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment } from 'semantic-ui-react';

const PasswordResetForm = ({ confirmResetPassword, resetHash }) => {
  const [email, setEmail] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isPasswordConfirmationValid, setIsPasswordConfirmationValid] = useState(true);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const passwordChanged = data => {
    setNewPassword(data);
    setIsPasswordValid(true);
  };

  const passwordConfirmationChanged = data => {
    setPasswordConfirmation(data);
    setIsPasswordConfirmationValid(true);
  };

  const handleConfirm = async () => {
    const isValid = isEmailValid && isPasswordValid && isPasswordConfirmationValid;
    if (newPassword !== passwordConfirmation) {
      setIsPasswordConfirmationValid(false);
      return;
    }
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    await confirmResetPassword({ email, password: newPassword, passwordConfirmation, resetHash });
    setIsLoading(false);
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleConfirm}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="New password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => passwordChanged(ev.target.value)}
          onBlur={() => setIsPasswordValid(Boolean(newPassword))}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="New password confirmation"
          type="password"
          error={!isPasswordConfirmationValid}
          onChange={ev => passwordConfirmationChanged(ev.target.value)}
          onBlur={() => setIsPasswordConfirmationValid(Boolean(newPassword))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Confirm
        </Button>
      </Segment>
    </Form>
  );
};

PasswordResetForm.propTypes = {
  confirmResetPassword: PropTypes.func.isRequired,
  resetHash: PropTypes.string.isRequired
};

export default PasswordResetForm;
