import React, { useEffect, useRef, useState } from 'react';
import { Icon, Image } from 'semantic-ui-react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserImgLink } from '../../helpers/imageHelper';
import { getCroppedImg } from './renderCroppedImg';
import { newAvatarSet } from '../../containers/Profile/actions';

const AvatarEditor = ({ initialImg, isEdited, newAvatarSet: _newAvatarSet }) => {
  const [initialImage, setInitialImage] = useState(null);
  const [crop, setCrop] = useState({ width: 200, aspect: 1 });
  const [completedCrop, setCompletedCrop] = useState({ width: 200, aspect: 1 });
  const [onCropActionCompleteEvent, setOnCropActionCompleteEvent] = useState(false);
  const canvas = useRef(document.createElement('canvas'));
  const imgRef = useRef(null);

  useEffect(() => {
    if (initialImage && !isEdited) {
      setInitialImage(null);
    }

    if (onCropActionCompleteEvent) {
      setOnCropActionCompleteEvent(false);
      _newAvatarSet(getCroppedImg(imgRef.current, completedCrop, canvas.current));
    }
  }, [initialImage, isEdited, onCropActionCompleteEvent, _newAvatarSet, completedCrop, canvas]);

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        setInitialImage(reader.result);
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onImageLoaded = i => {
    imgRef.current = i;
    setOnCropActionCompleteEvent(true);
  };

  const onCropActionComplete = c => {
    setCompletedCrop(c);
    setOnCropActionCompleteEvent(true);
  };

  return (
    <>
      {!initialImage && (isEdited ? (
        <div style={{ position: 'relative' }}>
          <Image centered src={getUserImgLink(initialImg)} size="medium" circular />
          <label htmlFor="upload" style={{ position: 'absolute', top: '10%', right: '10%' }}>
            <Icon
              name="camera"
              size="big"
              link
              circular
              inverted
            />
            <input
              type="file"
              id="upload"
              style={{ display: 'none' }}
              accept="image/*"
              onChange={onSelectFile}
            />
          </label>
        </div>
      )
        : (<Image centered src={getUserImgLink(initialImg)} size="medium" circular />))}

      {initialImage && (
        <div style={{ width: '300px', height: '300px', background: '#6e6e6e', borderRadius: '10px' }}>
          <ReactCrop
            src={initialImage}
            crop={crop}
            onChange={newCrop => setCrop(newCrop)}
            onImageLoaded={onImageLoaded}
            maxWidth={200}
            maxHeight={200}
            onComplete={onCropActionComplete}
            circularCrop
            keepSelection
            style={{ borderRadius: '10px' }}
          />
        </div>
      )}
    </>
  );
};

AvatarEditor.propTypes = {
  initialImg: PropTypes.objectOf(PropTypes.any),
  isEdited: PropTypes.bool,
  newAvatarSet: PropTypes.func.isRequired
};

AvatarEditor.defaultProps = {
  initialImg: null,
  isEdited: false
};

const actions = { newAvatarSet };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(AvatarEditor);
