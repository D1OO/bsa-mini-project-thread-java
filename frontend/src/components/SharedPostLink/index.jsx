import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Icon, Input, Modal, Popup, Transition } from 'semantic-ui-react';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, whoShared, close, shareByMail,
  submit, validationResults, resetValidationResults }) => {
  const [copied, setCopied] = useState(false);
  const [email, setEmail] = useState(null);

  const emailNotValid = Boolean(validationResults?.shareByEmailForm.filter(err => err.field === 'email'));
  const link = `${window.location.origin}/share/${postId}`;

  let input = useRef();
  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied(true);
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          value={link}
          ref={ref => { input = ref; }}
          action={(
            <>
              <Button
                type="submit"
                color="teal"
                labelPosition="right"
                icon="copy"
                content="Copy"
                onClick={copyToClipboard}
              />
              <Popup
                style={{ width: '35rem', fontWeight: 'bold' }}
                flowing
                wide="very"
                inverted
                on="click"
                position="top right"
                onClose={resetValidationResults}
                trigger={(
                  <Button
                    content="Mail"
                    attached="left"
                    color="black"
                    labelPosition="right"
                    icon="mail"
                    onClick={resetValidationResults}
                  />
                )}
              >
                <Form onSubmit={() => shareByMail(email, link, whoShared)}>
                  <Popup
                    open={emailNotValid}
                    style={{ color: '#ff6363' }}
                    trigger={(
                      <Form.Input
                        placeholder="Receiver..."
                        fluid
                        focus
                        onChange={e => setEmail(e.target.value)}
                        onClick={resetValidationResults}
                        icon="at"
                        iconPosition="left"
                        error={emailNotValid}
                        action={(
                          <Button
                            type="submit"
                            positive={submit}
                            style={{ position: 'relative', width: '100px' }}
                          >
                            <Transition visible={submit} animation="fade" duration={{ hide: 50, show: 500 }}>
                              <Button.Content style={{ position: 'absolute' }}>
                                Success
                                <Icon name="check" />
                              </Button.Content>
                            </Transition>
                            <Transition visible={!submit} animation="fade" duration={{ hide: 50, show: 250 }}>
                              <Button.Content style={{ position: 'absolute' }}>
                                <Icon name="envelope square" />
                                Send
                              </Button.Content>
                            </Transition>
                          </Button>
                        )}
                      />
                    )}
                  >
                    Enter a valid email value
                  </Popup>
                </Form>
              </Popup>
            </>
          )}
        />
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  whoShared: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  resetValidationResults: PropTypes.func.isRequired,
  shareByMail: PropTypes.func.isRequired,
  submit: PropTypes.bool.isRequired,
  validationResults: PropTypes.objectOf(PropTypes.any)
};

SharedPostLink.defaultProps = {
  validationResults: null
};

export default SharedPostLink;
