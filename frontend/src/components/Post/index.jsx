import React from 'react';
import PropTypes from 'prop-types';
import { Card, Icon, Image, Label } from 'semantic-ui-react';
import moment from 'moment';
import styles from './styles.module.scss';
import EditPostBodyForm from '../EditedPostBody';
import LikedByPopUp from '../LikedByPopUp';

const Post = ({ post, likePost, dislikePost, loadMoreLikes, toggleExpandedPost, sharePost, removePost,
  isEdited, isSaved, editPost, cancelEditPost, saveEditedPost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    updatedAt
  } = post;

  const createdDate = moment(createdAt).fromNow();
  const editedDate = moment(updatedAt).fromNow();

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {createdDate}
          </span>
          {updatedAt !== createdAt
            && (
              <span className="date" style={{ color: '#858585' }}>
                edited
                {' '}
                {editedDate}
              </span>
            )}
        </Card.Meta>
        <Card.Description>
          <div>
            {isEdited
              ? (<EditPostBodyForm post={post} saveEditedPost={saveEditedPost} cancelEditPost={cancelEditPost} />)
              : (
                <span className="postBody" style={{ whiteSpace: 'pre-line' }}>
                  { body }
                </span>
              )}
          </div>
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        {isSaved
        && (
          <span style={{ color: '#5ca54d' }}>
            Changes are saved
          </span>
        )}
        {likeCount > 0
          ? (
            <LikedByPopUp
              likedItemId={post.id}
              loadMoreLikes={loadMoreLikes}
              hoverableElement={(
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id, user.id)}>
                  <Icon name="thumbs up" />
                  {likeCount}
                </Label>
              )}
            />
          )
          : (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id, user.id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          )}
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {removePost != null
        && (
          <div style={{ float: 'right' }}>
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => editPost(id)}
            >
              <Icon name="edit" />
            </Label>
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => removePost(id)}
            >
              <Icon name="remove" />
            </Label>
          </div>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  loadMoreLikes: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  removePost: PropTypes.func,
  editPost: PropTypes.func,
  cancelEditPost: PropTypes.func,
  saveEditedPost: PropTypes.func,
  isEdited: PropTypes.bool,
  isSaved: PropTypes.bool
};

Post.defaultProps = {
  removePost: null,
  editPost: null,
  isEdited: false,
  isSaved: false,
  cancelEditPost: undefined,
  saveEditedPost: undefined
};

export default Post;
