import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Stomp } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, applyPost }) => {
  const [stompClient] = useState(Stomp.over(() => new SockJS('/ws')));

  useEffect(() => {
    if (!user) {
      return undefined;
    }

    const { id } = user;

    stompClient.debug = () => { };
    stompClient.connect({}, () => {
      stompClient.subscribe(`/user/${id}/queue/text-info`, message => {
        NotificationManager.info(`${message.body}`);
      });

      stompClient.subscribe(`/user/${id}/queue/text-success`, message => {
        NotificationManager.success(`${message.body}`);
      });

      stompClient.subscribe(`/user/${id}/queue/text-error`, message => {
        NotificationManager.error(`${message.body}`);
      });

      stompClient.subscribe('/topic/new_post', message => {
        const post = JSON.parse(message.body);
        if (post.userId !== id) {
          applyPost(post.id);
        }
      });
    });

    return () => {
      stompClient.disconnect();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired
};

export default Notifications;
