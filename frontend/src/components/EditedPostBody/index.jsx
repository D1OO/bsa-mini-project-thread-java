import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, TextArea } from 'semantic-ui-react';

class EditPostBodyForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      body: props.post.body,
      loading: false
    };
    this.properties = {
      post: props.post,
      cancelEditPost: props.cancelEditPost,
      saveEditedPost: props.saveEditedPost
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ body: event.target.value });
  }

  handleSubmit(event) {
    const { body } = this.state;
    const { post, saveEditedPost } = this.properties;
    post.body = body;
    saveEditedPost(post);
    event.preventDefault();
  }

  render() {
    const { body, loading } = this.state;
    const { cancelEditPost } = this.properties;
    return (
      <Form onSubmit={this.handleSubmit}>
        <TextArea value={body} onChange={this.handleChange} style={{ marginBottom: '1rem' }}>
          {body}
        </TextArea>
        <Button.Group floated="right" size="small">
          <Button
            type="submit"
            compact
            positive={!loading}
            loading={loading}
            onClick={() => this.setState({ loading: true })}
          >
            Save
          </Button>
          <Button.Or />
          <Button compact onClick={() => cancelEditPost()}> Cancel </Button>
        </Button.Group>
      </Form>
    );
  }
}

EditPostBodyForm.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  cancelEditPost: PropTypes.func.isRequired,
  saveEditedPost: PropTypes.func.isRequired
};

export default EditPostBodyForm;
