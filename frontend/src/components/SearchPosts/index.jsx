import { debounce } from 'lodash';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
  Button,
  Grid, Icon,
  Image,
  Input,
  Label,
  List,
  Loader,
  Placeholder,
  Segment,
  Transition
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import moment from 'moment';
import { doSearch, resetSearchState, searchFinished, searchStarted } from '../../containers/Thread/actions';
import styles from './styles.module.scss';
import { getUserImgLink } from '../../helpers/imageHelper';
import useOutsideClick from './useOutsideClick';

const SearchPosts = ({
  doSearch: _doSearch,
  resetSearchState: _resetSearchState,
  searchResults,
  hasMoreSearchResults,
  searchStarted: _searchStarted,
  searchFinished: _searchFinished,
  searchInProgress,
  findPostsByUsername,
  findPostsByContent,
  resetSearch,
  toggleExpandedPost
}) => {
  const [searchQuery, setSearchQuery] = useState(null);
  const [isSearchResultsVisible, setIsSearchResultsVisible] = useState(false);

  const [isByNameActive, setIsByNameActive] = useState(true);

  const searchInput = useRef(null);
  const typeToggled = useRef(null);

  const searchType1Button = useRef(null);
  const searchType2Button = useRef(null);

  useOutsideClick([searchType1Button.current?.ref, searchType2Button.current?.ref], () => {
    searchInput.current.inputRef.current.blur();
    setIsSearchResultsVisible(false);
  });

  useEffect(() => {
    if (typeToggled.current) {
      typeToggled.current = false;
      searchInput.current.focus();
    }
  });

  const debounceLoadSearchResult = useCallback(debounce((value, type) => {
    _resetSearchState();
    _doSearch(value, type ? 'name' : 'content');
  }, 400), []);

  const handleInputChange = e => {
    const { value: query } = e.target;
    setSearchQuery(query);
    if (query.length < 1) {
      debounceLoadSearchResult.cancel();
      setSearchQuery(null);
      setIsSearchResultsVisible(false);
      _searchFinished();
      resetSearch();
    } else {
      _searchStarted();
      setIsSearchResultsVisible(true);
      debounceLoadSearchResult(query, isByNameActive);
    }
  };

  const handleKeyDown = e => {
    if (e.key === 'Enter') {
      if (isByNameActive) findPostsByUsername(searchQuery);
      else findPostsByContent(searchQuery);
      setIsSearchResultsVisible(false);
      searchInput.current.inputRef.current.blur();
    }
  };

  const handleActionCLick = () => {
    if (searchQuery?.length) {
      if (isByNameActive) findPostsByUsername(searchQuery);
      else findPostsByContent(searchQuery);
    } else {
      resetSearch();
    }
  };

  const handleFocus = () => {
    if (searchQuery?.length > 0) {
      handleInputChange({ target: { value: searchQuery } });
    }
  };

  const handleResultSelectName = name => {
    setSearchQuery(name);
    findPostsByUsername(name);
  };

  const handleResultSelectContent = postId => {
    toggleExpandedPost(postId);
  };

  const handleTypeSwitch = type => {
    if (isByNameActive !== type) {
      typeToggled.current = true;
      setIsByNameActive(type);
      if (searchQuery?.length > 0) _searchStarted();
    }
  };

  return (
    <Grid style={{ padding: '1em' }} verticalAlign="middle">
      <Grid.Column width={12} style={{ position: 'relative' }}>
        <Input
          placeholder="Search..."
          loading={searchInProgress}
          onChange={handleInputChange}
          onKeyDown={handleKeyDown}
          onFocus={handleFocus}
          action={<Button icon="search" loading={searchInProgress} onClick={handleActionCLick} />}
          value={searchQuery}
          fluid
          ref={searchInput}
        />
        <Transition
          visible={isSearchResultsVisible}
          animation="fade"
          duration={{ hide: 300, show: 300 }}
          unmountOnHide="true"
        >
          <div className={styles.searchBarPopup}>
            {searchInProgress && (
              <Segment raised>
                <List verticalAlign="middle" size="large">
                  <List.Item className={styles.postAuthorPlaceholder}>
                    <Placeholder className={styles.placeholderImage}>
                      <Placeholder.Image />
                    </Placeholder>
                    <Placeholder className={styles.placeholderUsername}>
                      <Placeholder.Line />
                    </Placeholder>
                  </List.Item>
                </List>
              </Segment>
            )}
            {!searchInProgress && (searchResults.length > 0 ? (
              <Segment className={styles.searchResults} raised>
                <InfiniteScroll
                  useWindow={false}
                  pageStart={0}
                  threshold={10}
                  loadMore={() => _doSearch(searchQuery, isByNameActive ? 'name' : 'content')}
                  hasMore={hasMoreSearchResults}
                  loader={<Loader active inline="centered" key={0} />}
                >
                  {isByNameActive
                    ? (
                      <List verticalAlign="middle" selection size="large">
                        {searchResults.map(l => (
                          <List.Item
                            as="a"
                            className={styles.searchItem}
                            onClick={() => handleResultSelectName(l.username)}
                            key={l.username}
                          >
                            <Image avatar src={getUserImgLink(l.avatar)} />
                            <List.Content>
                              <List.Header>
                                <Label size="large" className={styles.searchResultItem}>{l.username}</Label>
                              </List.Header>
                            </List.Content>
                          </List.Item>
                        ))}
                      </List>
                    )
                    : (
                      <List verticalAlign="middle" selection size="large" divided>
                        {searchResults.map(l => (
                          <List.Item
                            as="a"
                            className={styles.searchItem}
                            onClick={() => handleResultSelectContent(l.id)}
                            key={l.id}
                          >
                            <List.Content>
                              <Grid style={{ padding: '1em', paddingBottom: 0 }}>
                                <div style={{ padding: 0 }}>
                                  <Label size="large" className={styles.searchResultItem}>
                                    {l.body.length > 60 ? `${l.body.substring(0, 60)}...` : l.body}
                                  </Label>
                                </div>
                                <Grid.Row
                                  style={{ display: 'inline-flex', alignItems: 'center' }}
                                >
                                  <Label color="black">{l.authorUsername}</Label>
                                  <Grid.Column textAlign="right" style={{ flexGrow: 1 }}>
                                    <Grid.Row
                                      style={{ display: 'inline-flex',
                                        alignItems: 'center',
                                        justifyContent: 'right' }}
                                      stretched
                                    >
                                      <Label size="small" className={styles.searchResultItem}>
                                        <Icon name="like" color="red" />
                                        {l.likeCount}
                                      </Label>
                                      <Label size="small" className={styles.searchResultItem}>
                                        <Icon name="comment alternate outline" color="black" />
                                        {l.commentCount}
                                      </Label>
                                      <Label
                                        size="small"
                                        className={styles.searchResultItem}
                                        style={{ maxWidth: '60%' }}
                                      >
                                        {moment(l.createdAt).calendar()}
                                      </Label>
                                    </Grid.Row>

                                  </Grid.Column>
                                </Grid.Row>
                              </Grid>
                            </List.Content>
                          </List.Item>
                        ))}
                      </List>
                    )}
                </InfiniteScroll>

              </Segment>
            ) : (
              <Segment raised>
                <Label>No elements found</Label>
              </Segment>
            ))}
          </div>
        </Transition>
      </Grid.Column>
      <Grid.Column textAlign="center" style={{ flexGrow: 1 }}>
        <Grid.Row>
          <Button.Group size="tiny" compact>
            <Button
              content="Name"
              type="button"
              onClick={() => handleTypeSwitch(true)}
              active={isByNameActive}
              className="theme"
              style={{ paddingInline: '0.9em' }}
              ref={searchType1Button}
            />
            <Button
              content="Content"
              type="button"
              onClick={() => handleTypeSwitch(false)}
              active={!isByNameActive}
              className="theme"
              style={{ paddingInline: '0.9em' }}
              ref={searchType2Button}
            />
          </Button.Group>
        </Grid.Row>
      </Grid.Column>
    </Grid>
  );
};

SearchPosts.propTypes = {
  doSearch: PropTypes.func.isRequired,
  resetSearchState: PropTypes.func.isRequired,
  searchStarted: PropTypes.func.isRequired,
  searchFinished: PropTypes.func.isRequired,
  findPostsByUsername: PropTypes.func.isRequired,
  findPostsByContent: PropTypes.func.isRequired,
  resetSearch: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  searchResults: PropTypes.arrayOf(PropTypes.any),
  hasMoreSearchResults: PropTypes.bool,
  searchInProgress: PropTypes.bool
};

SearchPosts.defaultProps = {
  searchResults: [],
  hasMoreSearchResults: false,
  searchInProgress: false
};

const mapStateToProps = rootState => ({
  searchResults: rootState.posts.searchResults,
  hasMoreSearchResults: rootState.posts.hasMoreSearchResults,
  searchInProgress: rootState.posts.searchInProgress
});

const actions = { doSearch, resetSearchState, searchStarted, searchFinished };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPosts);
