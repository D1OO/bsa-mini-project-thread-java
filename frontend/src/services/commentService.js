import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'PUT',
    request
  });
  return response.json();
};

export const removeComment = async id => {
  await callWebApi({
    endpoint: '/api/comments',
    type: 'DELETE',
    request: id
  });
};

export const saveEditedComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const likeComment = async (commentId, commentAuthorId) => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'PUT',
    request: {
      commentId,
      commentAuthorId,
      isLike: true
    }
  });
  return response.json();
};

export const dislikeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'PUT',
    request: {
      commentId,
      isLike: false
    }
  });
  return response.json();
};

export const getAllLikes = async filter => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'GET',
    query: filter
  });
  return response.json();
};
