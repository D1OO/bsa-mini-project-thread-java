import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const updateUserProfile = async request => {
  const response = await callWebApi({
    endpoint: '/api/user/update/profile',
    type: 'POST',
    request
  });
  return response.json();
};

export const updateUserStatus = async request => {
  const response = await callWebApi({
    endpoint: '/api/user/update/status',
    type: 'POST',
    request
  });
  return response.json();
};

export const resetPasswordRequested = async request => {
  await callWebApi({
    endpoint: '/api/user/reset-password',
    type: 'POST',
    request
  });
  return true;
};

export const confirmResetPassword = async request => {
  await callWebApi({
    endpoint: '/api/auth/reset-password',
    type: 'POST',
    request
  });
  return true;
};
