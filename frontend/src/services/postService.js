import callWebApi from 'src/helpers/webApiHelper';

export const getAllPosts = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const addPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts/add',
    type: 'POST',
    request
  });
  return response.json();
};

export const getPost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const likePost = async (postId, postAuthorId) => {
  const response = await callWebApi({
    endpoint: '/api/postreaction',
    type: 'PUT',
    request: {
      postId,
      postAuthorId,
      postLink: `${window.location.origin}/share/${postId}`,
      isLike: true
    }
  });
  return response.json();
};

export const dislikePost = async postId => {
  const response = await callWebApi({
    endpoint: '/api/postreaction',
    type: 'PUT',
    request: {
      postId,
      isLike: false
    }
  });
  return response.json();
};

export const removePost = async postId => {
  await callWebApi({
    endpoint: `/api/posts/remove/${postId}`,
    type: 'DELETE',
    request: {
      postId
    }
  });
};

export const saveEditedPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts/save-edited',
    type: 'POST',
    request
  });
  return response.json();
};

export const getAllLikes = async filter => {
  const response = await callWebApi({
    endpoint: '/api/postreaction',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const shareByMail = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts/share-by-mail',
    type: 'POST',
    request
  });
  return response.json();
};

export const getSearchedByAuthorSuggestions = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts/searched-by-author',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const getSearchedByContentSuggestions = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts/searched-by-content',
    type: 'GET',
    query: filter
  });
  return response.json();
};

// should be replaced by appropriate function
export const getPostByHash = async hash => getPost(hash);

// function hash(str) {
//   const len = str.length;
//   let hashh = 5381;
//   for (let idx = 0; idx < len; idx + 1) {
//     hashh = 33 * hashh + str.charCodeAt(idx);
//   }
//   return hashh;
// }
