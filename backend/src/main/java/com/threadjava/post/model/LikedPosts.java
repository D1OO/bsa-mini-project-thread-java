package com.threadjava.post.model;

import lombok.Data;
import org.hibernate.annotations.Subselect;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

/**
 * 19.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Entity
@Subselect("SELECT post_id, user_id FROM post_reactions WHERE is_like = true")
public class LikedPosts {
    @Id
    private String postId;
    private UUID userId;
}
