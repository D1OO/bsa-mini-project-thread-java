package com.threadjava.post;

import com.threadjava.config.AsyncTasks;
import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getAuthUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private AsyncTasks asyncTasks;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue = "0") Integer from,
                                 @RequestParam(defaultValue = "10") Integer count,
                                 @RequestParam(required = false) String username,
                                 @RequestParam(required = false) UUID excludedUserId,
                                 @RequestParam(required = false) UUID likedByUserId,
                                 @RequestParam(required = false) String content) {
        return postsService.getAllPosts(from, count, username, excludedUserId, likedByUserId, content);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping("/add")
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getAuthUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @DeleteMapping("/remove/{id}")
    public void remove(@PathVariable UUID id) {
        postsService.remove(id);
    }

    @PostMapping("/save-edited")
    public PostDetailsDto saveEdited(@RequestBody PostDetailsDto postDto) {
        postDto.setUpdatedAt(postsService.saveEdited(postDto).getUpdatedAt());
        return postDto;
    }

    @PostMapping("/share-by-mail")
    public boolean shareByEmail(@RequestBody @Valid PostShareByMailDto postDto) {

        asyncTasks.sendEmail(postDto.getEmail(),
                "Thread: Someone shared a post with you",
                "Hello\n" + postDto.getWhoShared() + " shared this with you:\n\n" + postDto.getLink());
        return true;
    }

    @GetMapping("/searched-by-author")
    public Map<String, Object> findPostAuthors(@RequestParam(defaultValue = "0") Integer from,
                                               @RequestParam(defaultValue = "10") Integer count,
                                               @RequestParam(required = false) String username,
                                               @RequestParam(required = false) UUID excludedUserId,
                                               @RequestParam(required = false) UUID likedByUserId,
                                               @RequestParam(required = false) String query) {
        return postsService.findPostsAuthorsPage(query, from, count);
    }

    @GetMapping("/searched-by-content")
    public Map<String, Object> findPostsWithBody(@RequestParam(defaultValue = "0") Integer from,
                                               @RequestParam(defaultValue = "10") Integer count,
                                               @RequestParam(required = false) String username,
                                               @RequestParam(required = false) UUID excludedUserId,
                                               @RequestParam(required = false) UUID likedByUserId,
                                               @RequestParam(required = false) String query) {
        return postsService.findPostsByContentPage(query, from, count, username, excludedUserId, likedByUserId);
    }

}
