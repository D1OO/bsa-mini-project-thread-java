package com.threadjava.post;

import com.threadjava.post.dto.PostDetailsQueryResult;
import com.threadjava.post.dto.PostListQueryResult;
import com.threadjava.post.dto.SearchPostAuthorsQueryResult;
import com.threadjava.post.dto.SearchedPostDetailsQueryResult;
import com.threadjava.post.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostsRepository extends JpaRepository<Post, UUID> {

    @Query("SELECT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments), " +
            "p.createdAt, p.updatedAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE (cast(:username as string) is null OR p.user.username = :username) " +
            "AND (cast(:excludedUserId as string) is null OR p.user.id != :excludedUserId) " +
            "AND (cast(:likedByUserId as string) is null OR p.id IN (SELECT postId FROM LikedPosts WHERE userId = :likedByUserId)) " +
            "AND (cast(:body as string) is null OR LOWER(p.body) LIKE LOWER(CONCAT('%', cast(:body as string), '%')))  " +
            "order by p.createdAt desc")
    List<PostListQueryResult> findAllPosts(@Param("username") String username,
                                           @Param("excludedUserId") UUID excludedUserId,
                                           @Param("likedByUserId") UUID likedByUserId,
                                           @Param("body") String body,
                                           Pageable pageable);

    @Query("SELECT new com.threadjava.post.dto.PostDetailsQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments), " +
            "p.createdAt, p.updatedAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE p.id = :id")
    Optional<PostDetailsQueryResult> findPostById(@Param("id") UUID id);

    @Query("UPDATE Post SET removed = true WHERE id = :id ")
    @Modifying
    void softRemovePost(@Param("id") UUID id);

    @Query("SELECT DISTINCT new com.threadjava.post.dto.SearchPostAuthorsQueryResult(p.user.username, i) " +
            "FROM Post p " +
            "LEFT JOIN p.user.avatar i " +
            "WHERE lower(p.user.username) LIKE lower(concat('%', :name, '%')) ")
    Page<SearchPostAuthorsQueryResult> findPostsAuthorsPage(@Param("name") String name, Pageable pageable);

    @Query("SELECT DISTINCT new com.threadjava.post.dto.SearchedPostDetailsQueryResult(p.id, p.body, " +
            "COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0), " +
            "(SELECT COUNT(*) FROM p.comments), " +
            "p.createdAt, (cast(i.link as string) is null) , p.user.username) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "LEFT JOIN p.reactions pr " +
            "WHERE (cast(:username as string) is null OR p.user.username = :username) " +
            "AND (cast(:body as string) is null OR lower(p.body) LIKE lower(concat('%',:body,'%')))  " +
            "AND (cast(:excludedUserId as string) is null OR p.user.id != :excludedUserId) " +
            "AND (cast(:likedByUserId as string) is null OR p.id IN (SELECT postId FROM LikedPosts WHERE userId = :likedByUserId))" +
            "group by p.id, p.body, p.createdAt, p.user.username, i.link " +
            "order by p.createdAt desc")
    Page<SearchedPostDetailsQueryResult> findAllPostsSearc(@Param("body") String body,
                                                           @Param("username") String username,
                                                           @Param("excludedUserId") UUID excludedUserId,
                                                           @Param("likedByUserId") UUID likedByUserId,
                                                           Pageable pageable);
}