package com.threadjava.post;

import com.threadjava.comment.CommentMapper;
import com.threadjava.comment.CommentRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;

    public List<PostListDto> getAllPosts(Integer from, Integer count,
                                         String username, UUID excludedUserId, UUID likedByUserId, String body) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllPosts(username, excludedUserId, likedByUserId, body, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postQRToPostDetailsDto)
                .orElseThrow();

        var comments = commentRepository.findAllByPostIdAndRemovedFalse(id)
                .stream()
                .map(CommentMapper.MAPPER::commentToCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postCreationDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public PostDetailsDto saveEdited(PostDetailsDto postDto) {
        return PostMapper.MAPPER.postToPostDetailsDto(postsCrudRepository
                .save(PostMapper.MAPPER.postDetailsDtoToPost(postDto)));
    }

    @Transactional
    public void remove(UUID postId) {
        postsCrudRepository.softRemovePost(postId);
    }

    public Map<String, Object> findPostsAuthorsPage(String name, Integer from, Integer count) {
        Page<SearchPostAuthorsQueryResult> p = postsCrudRepository.findPostsAuthorsPage(name,
                PageRequest.of(from / count, count));

        return Map.of("hasMore", !p.isLast(), "data",p.getContent().stream()
                .map(PostMapper.MAPPER::searchPostAuthorsToDto));
    }

    public Map<String, Object> findPostsByContentPage(String body, Integer from, Integer count,
                                                      String username, UUID excludedUserId, UUID likedByUserId) {
        Page<SearchedPostDetailsQueryResult> p = postsCrudRepository
                .findAllPostsSearc(body, username, excludedUserId, likedByUserId,
                        PageRequest.of(from / count, count));

        return Map.of("hasMore", !p.isLast(), "data",p.getContent());
    }

}
