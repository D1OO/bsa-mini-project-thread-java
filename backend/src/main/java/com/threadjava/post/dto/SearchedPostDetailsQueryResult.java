package com.threadjava.post.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

/**
 * 29.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */

@Data
@AllArgsConstructor
public class SearchedPostDetailsQueryResult {
    private UUID id;
    private String body;
    private long likeCount;
    private long commentCount;
    private Date createdAt;
    private Boolean containsImage;
    private String authorUsername;
}
