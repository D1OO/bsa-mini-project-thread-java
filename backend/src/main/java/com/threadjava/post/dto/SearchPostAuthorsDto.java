package com.threadjava.post.dto;

import com.threadjava.image.dto.ImageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 28.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchPostAuthorsDto {
    private String username;
    private ImageDto avatar;
}
