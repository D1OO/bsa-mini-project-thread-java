package com.threadjava.post.dto;

import com.threadjava.image.model.Image;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 28.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class SearchPostAuthorsQueryResult {
    private String username;
    private Image image;
}
