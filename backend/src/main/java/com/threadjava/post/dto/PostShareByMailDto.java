package com.threadjava.post.dto;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * 21.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
public class PostShareByMailDto {
    @Valid
    @Email
    @NotEmpty
    private String email;
    private String link;
    private String whoShared;
}
