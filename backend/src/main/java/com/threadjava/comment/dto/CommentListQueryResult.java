package com.threadjava.comment.dto;

import com.threadjava.image.model.Image;
import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

/**
 * 17.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentListQueryResult {
    public UUID id;
    public Date createdAt;
    public UUID postId;
    public String body;
    public long likeCount;
    public long dislikeCount;
    public User user;
}
