package com.threadjava.comment.dto;

import com.threadjava.post.dto.PostUserDto;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class CommentDetailsDto {
    private UUID id;
    private UUID postId;
    private UUID postAuthorId;
    private Date createdAt;
    private String body;
    private long likeCount = 0;
    private long dislikeCount = 0;
    private PostUserDto user;
}
