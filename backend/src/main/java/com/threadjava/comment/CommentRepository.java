package com.threadjava.comment;

import com.threadjava.comment.dto.CommentListQueryResult;
import com.threadjava.comment.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    @Query("SELECT new com.threadjava.comment.dto.CommentListQueryResult(c.id," +
            "c.createdAt, c.post.id, c.body," +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 0 ELSE 1 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            " c.user)" +
            "FROM Comment c " +
            "WHERE c.post.id = :postId " +
            "ORDER by c.createdAt desc" )
    List<CommentListQueryResult> findAllByPostIdAndRemovedFalse(UUID postId);

    @Query("UPDATE Comment SET removed = true WHERE id = :id ")
    @Modifying
    void softDeleteById(UUID id);
}