package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.threadjava.auth.TokenService.getAuthUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public CommentDetailsDto put(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getAuthUserId());
        var createdComm = commentService.create(commentDto);

        if (!commentDto.getPostAuthorId().equals(commentDto.getUserId())) {
            template.convertAndSendToUser(commentDto.getPostAuthorId().toString(),
                    "/queue/text-info", buildNewCommentMessage(commentDto));
        }
        return createdComm;
    }

    @DeleteMapping
    public void delete(@RequestBody UUID id) {
        commentService.delete(id);
    }

    @PostMapping
    public CommentDetailsDto post(@RequestBody CommentDetailsDto commentDto) {
        return commentService.update(commentDto);
    }

    private String buildNewCommentMessage(CommentSaveDto commentDto) {
        return String.format("New comment under your post:\n%s",
                (commentDto.getBody().length() < 50) ?
                        commentDto.getBody() :
                        new StringBuilder().append(commentDto.getBody(), 0, 25).append("\n")
                                .append(commentDto.getBody(), 25, 50).append("..."));
    }
}
