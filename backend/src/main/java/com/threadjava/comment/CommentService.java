package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.PostsRepository;
import com.threadjava.post.model.Post;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private PostsRepository postsRepository;
    @PersistenceContext
    EntityManager entityManager;

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        Comment saved = commentRepository.save(CommentMapper.MAPPER.commentSaveDtoToModel(commentDto));
        saved.setUser(usersRepository.findById(saved.getUser().getId())
                .orElseThrow(() -> new UsernameNotFoundException("")));
        return CommentMapper.MAPPER.commentToCommentDetailsDto(saved);
    }

    public CommentDetailsDto update(CommentDetailsDto commentDto){
        Comment comment = CommentMapper.MAPPER.commentDtoToEntity(commentDto);
        comment.setPost(entityManager.getReference(Post.class, commentDto.getPostId()));
        return CommentMapper.MAPPER.commentToCommentDetailsDto(commentRepository.save(comment));
    }

    @Transactional
    public void delete(UUID id) {
        commentRepository.softDeleteById(id);
    }
}
