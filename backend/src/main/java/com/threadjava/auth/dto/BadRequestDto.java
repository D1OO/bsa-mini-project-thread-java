package com.threadjava.auth.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 26.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class BadRequestDto {
    String error;
}
