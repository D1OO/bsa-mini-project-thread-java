package com.threadjava.auth;

import com.threadjava.auth.dto.AuthUserDTO;
import com.threadjava.auth.dto.BadRequestDto;
import com.threadjava.auth.dto.UserLoginDTO;
import com.threadjava.auth.dto.UserRegisterDto;
import com.threadjava.users.dto.ResetPasswordDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping("/register")
    public AuthUserDTO signUp(@RequestBody UserRegisterDto user) throws Exception {
        return authService.register(user);
    }

    @PostMapping("/login")
    public AuthUserDTO login(@RequestBody UserLoginDTO user) throws Exception {
        return authService.login(user);
    }

    @PostMapping("/reset-password")
    public ResponseEntity<Void> updateUserPassword(@RequestBody @Valid ResetPasswordDto resetPasswordDto) {
        return new ResponseEntity<>(authService
                .processPasswordReset(resetPasswordDto) ? HttpStatus.OK : HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<BadRequestDto> handleException() {
        return new ResponseEntity<>(new BadRequestDto("User not found"), HttpStatus.BAD_REQUEST);
    }
}
