package com.threadjava.users.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.util.UUID;

/**
 * 23.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserStatusDto {
    private UUID id;
    @Length(max = 30)
    private String status;
}
