package com.threadjava.users.dto;

import com.threadjava.image.dto.ImageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Pattern;
import java.util.UUID;

/**
 * 22.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileUpdatedDto {
    private UUID id;
    @Pattern(regexp = "^[0-9a-zA-Z]{3,15}$")
    private String username;
    @Length(max = 30)
    private String status;

    private String newAvatar;
    private ImageDto uploadedAvatar;
}
