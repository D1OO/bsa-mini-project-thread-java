package com.threadjava.users.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import java.util.UUID;

/**
 * 25.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResetPasswordDto {
    private UUID id;
    private UUID resetHash;
    @Email
    private String email;
    private String password;
}
