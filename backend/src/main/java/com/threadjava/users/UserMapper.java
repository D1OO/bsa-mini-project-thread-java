package com.threadjava.users;

import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserProfileUpdatedDto;
import com.threadjava.users.dto.UserStatusDto;
import com.threadjava.users.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper MAPPER = Mappers.getMapper( UserMapper.class );

    @Mapping(source = "avatar", target = "image")
    UserDetailsDto userToUserDetailsDto(User user);

    @Mapping(source = "uploadedAvatar", target = "avatar", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void fromDto(UserProfileUpdatedDto dto, @MappingTarget User entity);

    void fromDto(UserStatusDto dto, @MappingTarget User entity);
}
