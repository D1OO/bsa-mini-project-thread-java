package com.threadjava.users;

import com.threadjava.config.AsyncTasks;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserProfileUpdatedDto;
import com.threadjava.users.dto.UserStatusDto;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getAuthUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UsersService userDetailsService;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private AsyncTasks asyncTasks;

    @Value(value = "${frontend.url}")
    private String FRONTEND_URL;

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getAuthUserId());
    }

    @PostMapping("/update/profile")
    public UserDetailsDto updateUserProfile(@RequestBody @Valid UserProfileUpdatedDto userProfileUpdatedDto) throws IOException {
        userProfileUpdatedDto.setId(getAuthUserId());
        UserDetailsDto updated = userDetailsService.updateUserProfile(userProfileUpdatedDto);
        template.convertAndSendToUser(getAuthUserId().toString(),
                "/queue/text-success", "Profile updated");
        return updated;
    }

    @PostMapping("/update/status")
    public UserDetailsDto updateUserStatus(@RequestBody @Valid UserStatusDto userStatusDto) {
        userStatusDto.setId(getAuthUserId());
        UserDetailsDto updated = userDetailsService.updateUserStatus(userStatusDto);
        template.convertAndSendToUser(getAuthUserId().toString(),
                "/queue/text-success", "Status updated");
        return updated;
    }

    @PostMapping("/reset-password")
    public ResponseEntity<Void> resetPasswordRequest(@RequestBody String email) {
        Map.Entry<UUID, User> passwordResetHashAndUser =  userDetailsService
                .processResetPasswordRequest(email.replace("\"", ""));

        asyncTasks.sendEmail(passwordResetHashAndUser.getValue().getEmail(),
                "Thread: Password reset procedure",
                "Hello " + passwordResetHashAndUser.getValue().getUsername() + ",\n\n" +
                        "Proceed to a following link to complete your password reset procedure:\n\n" +
                        FRONTEND_URL + "reset-password/" + passwordResetHashAndUser.getKey());

        template.convertAndSendToUser(getAuthUserId().toString(), "/queue/text-info",
                "A message has been sent to your e-mail, check it to proceed");

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public void handleException(DataIntegrityViolationException e) {
        if (e.getCause().getCause().getMessage().contains("(username)=(")) {
            template.convertAndSendToUser(getAuthUserId().toString(), "/queue/text-error",
                    "This username is already taken");
        }
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Void> handleException() {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
