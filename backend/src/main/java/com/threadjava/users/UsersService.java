package com.threadjava.users;

import com.threadjava.auth.model.AuthUser;
import com.threadjava.image.ImageService;
import com.threadjava.image.model.Image;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserProfileUpdatedDto;
import com.threadjava.users.dto.UserStatusDto;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;
import java.util.UUID;

@Service
public class UsersService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ImageService imageService;
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
        return usersRepository
                .findByEmail(email)
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public User getUserByEmail(String email) {
        return usersRepository
                .findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public UserDetailsDto getUserById(UUID id) {
        return usersRepository
                .findById(id)
                .map(UserMapper.MAPPER::userToUserDetailsDto)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public User save(User user) {
        return usersRepository.save(user);
    }

    public UserDetailsDto updateUserProfile(UserProfileUpdatedDto userProfileUpdatedDto) throws IOException {
        User user = usersRepository.findById(userProfileUpdatedDto.getId()).orElseThrow();

        if (userProfileUpdatedDto.getNewAvatar() != null) {
            userProfileUpdatedDto.setUploadedAvatar(imageService
                            .upload(userProfileUpdatedDto.getNewAvatar()));

            if (user.getAvatar() != null) entityManager.detach(user.getAvatar());
            user.setAvatar(new Image());
        }

        UserMapper.MAPPER.fromDto(userProfileUpdatedDto, user);
        User saved = save(user);
        return UserMapper.MAPPER.userToUserDetailsDto(saved);
    }

    public UserDetailsDto updateUserStatus(UserStatusDto userStatusDto) {
        User user = usersRepository.findById(userStatusDto.getId()).orElseThrow();
        UserMapper.MAPPER.fromDto(userStatusDto, user);
        return UserMapper.MAPPER.userToUserDetailsDto(save(user));
    }

    public Map.Entry<UUID, User> processResetPasswordRequest(@RequestBody String email) {
        User user = getUserByEmail(email);

        UUID passwordResetHash = UUID.randomUUID();
        user.setPasswordResetHash(passwordResetHash);
        usersRepository.save(user);

        return new AbstractMap.SimpleEntry<>(passwordResetHash, user);
    }
}