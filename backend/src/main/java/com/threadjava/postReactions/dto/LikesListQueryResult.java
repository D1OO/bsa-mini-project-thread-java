package com.threadjava.postReactions.dto;

import com.threadjava.image.model.Image;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 19.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LikesListQueryResult {
    private String username;
    private Image avatar;
}
