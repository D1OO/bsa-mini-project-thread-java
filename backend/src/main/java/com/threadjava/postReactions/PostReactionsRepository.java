package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.LikesListQueryResult;
import com.threadjava.postReactions.model.PostReaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface PostReactionsRepository extends CrudRepository<PostReaction, UUID> {
    @Query("SELECT r " +
            "FROM PostReaction r " +
            "WHERE r.user.id = :userId AND r.post.id = :postId ")
    Optional<PostReaction> getPostReaction(@Param("userId") UUID userId, @Param("postId") UUID postId);

    @Query("SELECT new com.threadjava.postReactions.dto.LikesListQueryResult(r.user.username, i) " +
            "FROM PostReaction r " +
            "LEFT JOIN r.user.avatar i " +
            "WHERE r.isLike = true AND r.post.id = :postId " +
            "order by r.createdAt desc")
    Page<LikesListQueryResult> findAllLikes(@Param("postId") UUID postId, Pageable pageable);
}