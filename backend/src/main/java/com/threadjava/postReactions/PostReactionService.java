package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.LikesListQueryResult;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class PostReactionService {
    @Autowired
    private PostReactionsRepository postReactionsRepository;

    public Page<LikesListQueryResult> getLikesPage(Integer from, Integer count, UUID postId) {
        return postReactionsRepository.findAllLikes(postId, PageRequest.of(from / count, count));
    }

    public Optional<ResponsePostReactionDto> setReaction(ReceivedPostReactionDto postReactionDto) {
        var presentReaction = postReactionsRepository.getPostReaction(postReactionDto.getUserId(), postReactionDto.getPostId());
        if (presentReaction.isPresent()) {
            var present = presentReaction.get();
            if (present.getIsLike() == postReactionDto.getIsLike()) {
                postReactionsRepository.deleteById(present.getId());
                return Optional.empty();
            } else {
                present.setIsLike(postReactionDto.getIsLike());
                var responseDto = PostReactionMapper.MAPPER.reactionToPostReactionDto(postReactionsRepository.save(present));
                responseDto.setSwitched(true);
                return Optional.of(responseDto);
            }
        } else {
            var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(postReactionDto);
            var result = postReactionsRepository.save(postReaction);
            return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
        }
    }
}
