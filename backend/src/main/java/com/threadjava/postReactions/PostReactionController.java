package com.threadjava.postReactions;

import com.threadjava.config.AsyncTasks;
import com.threadjava.postReactions.dto.LikesListQueryResult;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.users.UsersService;
import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getAuthUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {
    @Autowired
    private PostReactionService postReactionService;

    @Autowired
    private UsersService userService;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    AsyncTasks asyncTasks;

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction) {
        postReaction.setUserId(getAuthUserId());
        var reaction = postReactionService.setReaction(postReaction);

        // notify a user if someone (not himself) liked his post
        if (reaction.isPresent() && reaction.get().getIsLike()
                && !reaction.get().getUserId().equals(postReaction.getPostAuthorId()) ) {
            
            template.convertAndSendToUser(postReaction.getPostAuthorId().toString(),
                    "/queue/text-info",
                    "Your post was liked!");

            UserDetailsDto user = userService.getUserById(postReaction.getUserId());
            UserDetailsDto postAuthor = userService.getUserById(postReaction.getPostAuthorId());
            asyncTasks.sendEmail(postAuthor.getEmail(),
                    "Thread: Your post was liked",
                    "Hello\n Your post was liked by " + user.getUsername() + "\n\n" + postReaction.getPostLink());
        }
        return reaction;
    }

    @GetMapping
    public Map<String, Object> get(@RequestParam(defaultValue = "0") Integer from,
                                   @RequestParam(defaultValue = "5") Integer count,
                                   @RequestParam(required = false) UUID postId) {
        Page<LikesListQueryResult> page = postReactionService.getLikesPage(from, count, postId);
        return Map.of("hasMore", !page.isLast(), "data", page.getContent());
    }
}
