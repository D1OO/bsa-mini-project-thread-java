package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.postReactions.dto.LikesListQueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getAuthUserId;

/**
 * 17.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@RestController
@RequestMapping("/api/commentreaction")
public class CommentReactionController {
    @Autowired
    private CommentReactionService commentReactionService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponseCommentReactionDto> setReaction(@RequestBody ReceivedCommentReactionDto commentReaction) {
        commentReaction.setUserId(getAuthUserId());
        var reaction = commentReactionService.setReaction(commentReaction);

        if (reaction.isPresent() && reaction.get().getIsLike()
                && !commentReaction.getCommentAuthorId().equals(commentReaction.getUserId())) {
            template.convertAndSendToUser(commentReaction.getCommentAuthorId().toString(),
                    "/queue/text-info",
                    "Your comment was liked");
        }
        return reaction;
    }

    @GetMapping
    public Map<String, Object> get(@RequestParam(defaultValue = "0") Integer from,
                                   @RequestParam(defaultValue = "5") Integer count,
                                   @RequestParam(required = false) UUID commentId) {
        Page<LikesListQueryResult> page = commentReactionService.getLikesPage(from, count, commentId);
        return Map.of("hasMore", !page.isLast(), "data", page.getContent());
    }
}