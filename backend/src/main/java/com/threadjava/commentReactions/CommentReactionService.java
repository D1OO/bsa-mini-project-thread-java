package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.postReactions.dto.LikesListQueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * 17.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionRepository commentReactionRepository;

    public Page<LikesListQueryResult> getLikesPage(Integer from, Integer count, UUID postId) {
        return commentReactionRepository.findLikesPage(postId, PageRequest.of(from / count, count));
    }

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {
        var presentReaction = commentReactionRepository
                .getCommentReaction(commentReactionDto.getUserId(), commentReactionDto.getCommentId());
        if (presentReaction.isPresent()) {
            var present = presentReaction.get();
            if (present.getIsLike() == commentReactionDto.getIsLike()) {
                commentReactionRepository.deleteById(present.getId());
                return Optional.empty();
            } else {
                present.setIsLike(commentReactionDto.getIsLike());
                var responseDto = CommentReactionMapper.MAPPER
                        .reactionToCommentReactionDto(commentReactionRepository.save(present));
                responseDto.setSwitched(true);
                return Optional.of(responseDto);
            }
        } else {
            var commentReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
            var result = commentReactionRepository.save(commentReaction);
            return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
        }
    }
}
