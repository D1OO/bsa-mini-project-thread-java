package com.threadjava.commentReactions.dto;

import lombok.Data;

import java.util.UUID;

/**
 * 17.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
public class CommentReactionDto {
    private UUID id;
    private Boolean isLike;
}
