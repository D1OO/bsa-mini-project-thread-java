package com.threadjava.commentReactions.dto;

import lombok.Data;

import java.util.UUID;

/**
 * 17.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
public class ReceivedCommentReactionDto {
    private UUID commentId;
    private UUID commentAuthorId;
    private UUID userId;
    private Boolean isLike;
}
