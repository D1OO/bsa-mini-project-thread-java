package com.threadjava.commentReactions.dto;

import lombok.Data;

import java.util.UUID;

/**
 * 17.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
public class ResponseCommentReactionDto {
    private UUID id;
    private UUID commentId;
    private Boolean isLike;
    private Boolean switched;
    private UUID userId;
}