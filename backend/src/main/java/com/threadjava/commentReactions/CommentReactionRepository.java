package com.threadjava.commentReactions;

import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.postReactions.dto.LikesListQueryResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

/**
 * 17.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
public interface CommentReactionRepository extends CrudRepository<CommentReaction, UUID> {

    @Query("SELECT r " +
            "FROM CommentReaction r " +
            "WHERE r.user.id = :userId AND r.comment.id = :commentId")
    Optional<CommentReaction> getCommentReaction(@Param("userId") UUID userId, @Param("commentId") UUID commentId);

    @Query("SELECT new com.threadjava.postReactions.dto.LikesListQueryResult(r.user.username, i) " +
            "FROM CommentReaction r " +
            "LEFT JOIN r.user.avatar i " +
            "WHERE r.isLike = true AND r.comment.id = :commentId " +
            "order by r.createdAt desc")
    Page<LikesListQueryResult> findLikesPage(@Param("commentId") UUID commentId, Pageable pageable);

}
