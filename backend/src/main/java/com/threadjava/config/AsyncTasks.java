package com.threadjava.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 21.06.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Component
@Scope("singleton")
public class AsyncTasks {

    @Autowired
    private JavaMailSender javaMailSender;

    @Async
    public void sendEmail(String address, String subject, String body) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(address);
        msg.setSubject(subject);
        msg.setText(body);
        javaMailSender.send(msg);
    }

}
